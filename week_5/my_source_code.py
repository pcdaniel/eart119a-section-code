
def less_ten_count(input_list):
    """ return the number of numerics in a list with a value less than ten """
    under_ten = 0
    for num in input_list:
        if num < 10:
            under_ten = under_ten + 1

    return under_ten