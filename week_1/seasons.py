"""
Given a month (1 through 12), print the season.

# Spring == March through May
# Summer == June through August
# Fall == September through November
# Winter == December through Februrary

"""

month = 7


if month >= 3 & month < 6:
    season = "spring"

elif month >= 6 & month < 9:
    season = "summer"

elif month >= 9 & month < 12:
    season = "fall"

else:
    season = 'winter'

print(f"{month} is in {season}")