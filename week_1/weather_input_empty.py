"""
weather.py
eart119 - 2022

Exploring conditional logic and how to decide when to bring an umbrella.
"""

print("*"*20 + "\n"*3)

weather = input("What is weather report for today: ")

if weather == 'raining':
    # Do something here
    print("Don't forget your umbrella!")

elif weather == "snowing":
    print("Dont forget your Jacket!")

elif weather == "sunny":
    print("dont forget your sunglasses!")

else:
    print("It's not raining!")

print("\n"*3 +"*"*20 ) # These are just to make the printout look fancy
