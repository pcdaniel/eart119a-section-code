"""
Suppose a buoy measures sea surface temperature every 4 hours. Given a list of temperature measurements and a list of hours that correspond to each measurments:
1. find the maximum temperature from the list
2. find what hour the maxium took place
3. convert that time to days + hours remaining (hint: remember how we used // (floor) and % (modulus) operators)

"""
temperatures = [14.3940715005,14.2303281621,14.926319155,14.1038144322,13.9516660148,13.7202783938,13.1266461743,13.3588613088,14.6545417263,14.635458829,14.6038206613,14.5068303735,14.4837891657,14.5117615662,14.4412345833,14.3554295947,14.329173221,14.3601474617,14.6928244806,15.1608760331,15.2591185937,15.4490564794,15.4420522659,15.5922514407,15.6832835193,15.736238153,15.7414858316,15.4592465763,15.376447408,15.3515114624,15.3602079512,15.2095245747,15.1616700512,15.1514140659,14.9986314518,14.999834486,14.9219855005,14.8842581417,19.76362865,14.437596221,14.3548020238,14.3903775777,14.5570400981,14.5934106129,14.6334088619,14.6601200966,14.6062092664,14.6532054987]
hours_ellapsed = [0, 4, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56, 60, 64, 68, 72, 76, 80, 84, 88, 92, 96, 100, 104, 108, 112, 116, 120, 124, 128, 132, 136, 140, 144, 148, 152, 156, 160, 164, 168, 172, 176, 180, 184, 188]

current_max = 0
current_max_ix = 0

for i in range(len(temperatures)):
    current_temp = temperatures[i]
    if current_max < current_temp:
        current_max = current_temp
        current_max_ix = i


print("max temperature at: {} hours".format(hours_ellapsed[current_max_ix]))
print("max temperature at: {} days and {} hours".format(hours_ellapsed[current_max_ix]//24, hours_ellapsed[current_max_ix]%24))

print(current_max,current_max_ix)