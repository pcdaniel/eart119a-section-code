"""
Finish this code!

make a variable called pet_name that will evaluate if the name is equal to lloyd, george,norm, or ssomething else.
make a variable called pet_type and make they variable equal to dog, kitten, or cat, depending on what name is given.


I'll give you the first couple of lines:
"""

pet_name = input("What is your pets name? ")
pet_kind = "unkown"

if pet_name == "George":
    pet_kind = 'dog'

elif pet_name == "Lloyd":
    pet_kind = 'cat'

elif pet_name == "Norm":
    pet_kind = "kitten"


# We will use this print statement to test if things are working.
print("{} is a {}.".format(pet_name, pet_kind))

## Advanced: Can we loop through a list of pet names and print what kind they are?
