"""
weather.py
eart119 - 2022

Exploring conditional logic and how to decide when to bring an umbrella.
"""

print("*"*20 + "\n"*3)
weather = input("Weather: ")

if weather == 'raining':
    print("Don't forget to take and umbrella!")

elif weather == "sunny":
    print("dont forget your sunglasses")

elif weather == "foggy":
    print("Dont forget a sweater")

else:
    print("It's not raining, sunny, or foggy, but in santa cruz you never know")


print("\n"*3 +"*"*20 )