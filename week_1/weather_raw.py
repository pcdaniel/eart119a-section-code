"""
weather.py
eart119 - 2022

Exploring conditional logic and how to decide when to bring an umbrella.
"""

print("*"*20 + "\n"*3)

weather = input("What is weather report for today: ")
weather = weather.lower()

if (weather == "raining") | (weather == "snowing") :
    print("Take your umbrella")

elif weather == "snowing":
    print("Don't forget a sweater")

elif weather == "sunny":
    print("Wear a hat!")

else:
    print("Brush your teeth")

print("\n"*3 +"*"*20 ) # These are just to make the printout look fancy
