
pet_names = ["Norm", "Patrick", "Lloyd", "George", "Sammie"]

for pet_name in pet_names:
    if pet_name == "George":
        pet_kind = 'dog'

    elif pet_name == "Lloyd":
        pet_kind = 'cat'

    elif pet_name == "Norm":
        pet_kind = 'kitten'

    else:
        pet_kind = "unkown"

    print("{} is a {}.".format(pet_name, pet_kind))
