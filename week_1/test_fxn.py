


def count_numerics(items):
    count = 0
    total = 0
    for item in items:
        if type(item) == int:
            total += item
            count += 1
    if count == 0:
        return None
    else:
        return total

some_list = [3, 5, 2, "a", "b"]
print( count_numerics(some_list) )

some_list = ["3", "5", "2", "a", "b"]
print( count_numerics(some_list) )
