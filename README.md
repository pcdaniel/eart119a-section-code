# EART119a Section Code
__Patrick Daniel__  
Fall 2022


## Getting started

All of the coding examples for Section for EART119a are tracked within this repository.

I will try to keep them organized throughout the quarter and hopefully we will land on something that works well for everyone.

## How to access the files
Files are organized by each week:

| Week | Topics |
| --- | --- |
| [Week 0](./week_0) | `for` loops, `range()`, `enumerate()` |
| [Week 1](./week_1) | Logic, `if, elif, else`, `== !=, >=`, `& |`
| [Week 2](./week_2) | functions, calling, defining
| [Week 3](./week_3) | reading/writing csv, pandas, markdown
| [Week 4](./week_4) | more pandas, matplotlib, plotting
| [Week 5](./week_5) | midterm review
| [Week 6](./week_6) | Cartopy & Numpy
| [Week 7](./week_7) | Numpy: Broadcasting + Least Squares Fitting, Scipy, image manipulation

## Downloading
You can download each folder as a `.zip` file by selecting the download button in the upper right corner:

![](./imgs/download.png)
If you are more adventurous, you can [clone](https://github.com/git-guides/git-clone) the entire repository with the command `git clone https://git.ucsc.edu/pcdaniel/eart119a-section-code.git`.
